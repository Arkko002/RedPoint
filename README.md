[![Build Status](https://dev.azure.com/Arkko/RedPoint/_apis/build/status/Arkko002.RedPoint?branchName=master)](https://dev.azure.com/Arkko/RedPoint/_build/latest?definitionId=1&branchName=master)
# RedPoint

## Description
RedPoint is an online communicator that aims to provide users with the highest privacy possible and prevent any sort of information disclosure of it's users, while also being user-friendly.
It's a web project created in ASP.NET Core 2.1, using SignalR for the real time communication and Typescript for the UI.
RedPoint also provides a desktop client for Windows, which uses the Universal Windows Platform (UWP) and will be available in the Microsoft Store.


## Screenshots
//TODO 
